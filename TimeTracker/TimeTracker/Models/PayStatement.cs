﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTracker.Models
{
	public class PayStatement
	{
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public DateTime PayoutDate { get; set; }
		public double Amount { get; set; }
		public List<WorkItem> WorkItems { get; set; }
	}
}
