﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TimeTracker.Models;

namespace TimeTracker.Services.Statements
{
	public interface IStatementService
	{
		Task<List<PayStatement>> GetStatementHistoryAsync();
	}
}
