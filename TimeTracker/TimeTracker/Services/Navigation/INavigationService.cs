﻿using System.Threading.Tasks;
using TimeTracker.PageModels.Base;

namespace TimeTracker.Services.Navigation
{
	public interface INavigationService
	{
		/// <summary>
		/// Navigation method to push onto the navigation stack 
		/// </summary>
		/// <typeparam name="TPageModel"></typeparam>
		/// <param name="navigationData"></param>
		/// <param name="setRoot"></param>
		/// <returns></returns>
		Task NavigateToAsync<TPageModel>(object navigationData = null, bool setRoot = false)
			where TPageModel : PageModelBase;

		/// <summary>
		/// Navigation method to pop off of the navigation stack
		/// </summary>
		/// <returns></returns>
		Task GoBackAsync();
	}
}
