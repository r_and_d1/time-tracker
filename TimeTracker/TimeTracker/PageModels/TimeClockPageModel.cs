﻿using System;
using System.Collections.ObjectModel;
using System.Timers;
using System.Threading.Tasks;
using TimeTracker.Models;
using TimeTracker.PageModels.Base;
using TimeTracker.ViewModels.Buttons;
using TimeTracker.Services.Account;
using TimeTracker.Services.Work;

namespace TimeTracker.PageModels
{
	public class TimeClockPageModel : PageModelBase
	{
		private bool _isClockedIn;
		private TimeSpan _runningTotal;
		private DateTime _currentStartTime;
		private double _todaysEarnings;
		private Timer _timer;
		private ObservableCollection<WorkItem> _workItems;
		private IAccountService _accountService;
		private IWorkService _workService;
		private double _hourlyRate;
		ButtonModel _clockInOutButtonModel;

		public TimeClockPageModel(IAccountService accountService, IWorkService workService)
		{
			_accountService = accountService;
			_workService = workService;
			ClockInOutButtonModel = new ButtonModel("Clock In", OnClockInOutAction);
			_timer = new Timer();
			_timer.Interval = 1000;
			_timer.Enabled = false;
			_timer.Elapsed += _timer_Elaspsed;
		}


		public bool IsClockedIn
		{
			get => _isClockedIn;
			set => SetProperty(ref _isClockedIn, value);
		}

		public TimeSpan RunningTotal
		{
			get => _runningTotal;
			set => SetProperty(ref _runningTotal, value);
		}

		public DateTime CurrentStartTime
		{
			get => _currentStartTime;
			set => SetProperty(ref _currentStartTime, value);
		}

		public ObservableCollection<WorkItem> WorkItems
		{
			get => _workItems;
			set => SetProperty(ref _workItems, value);
		}

		public double TodaysEarnings
		{
			get => _todaysEarnings;
			set => SetProperty(ref _todaysEarnings, value);
		}

		public ButtonModel ClockInOutButtonModel
		{
			get => _clockInOutButtonModel;
			set => SetProperty(ref _clockInOutButtonModel, value);
		}

		private void _timer_Elaspsed(object sender, ElapsedEventArgs e)
		{
			RunningTotal += TimeSpan.FromSeconds(1);
		}

		public override async Task InitializeAsync(object navigationData)
		{
			RunningTotal = new TimeSpan();
			_hourlyRate = await _accountService.GetCurrentPayRateAsync();
			WorkItems = await _workService.GetTodaysWorkAsync();
			await base.InitializeAsync(navigationData);
		}

		private async void OnClockInOutAction()
		{
			if(IsClockedIn)
			{
				ClockInOutButtonModel.Text = "Clock In";
				_timer.Enabled = false;
				TodaysEarnings += _hourlyRate * RunningTotal.TotalHours;
				RunningTotal = TimeSpan.Zero;
				var item = new WorkItem
				{
					Start = CurrentStartTime,
					End = DateTime.Now
				};
				WorkItems.Insert(0, item);
				await _workService.LogWorkAsync(item);
			}
			else
			{
				CurrentStartTime = DateTime.Now;
				_timer.Enabled = true;
				ClockInOutButtonModel.Text = "Clock Out";
			}
			IsClockedIn = !IsClockedIn;
		}
	}
}
