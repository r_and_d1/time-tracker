﻿using System.Threading.Tasks;
using TimeTracker.PageModels.Base;

namespace TimeTracker.PageModels
{
	public class DashboardPageModel : PageModelBase
	{
		private ProfilePageModel _profilePageModel;
		public ProfilePageModel ProfilePageModel
		{
			get => _profilePageModel;
			set => SetProperty(ref _profilePageModel, value);
		}

		private SettingsPageModel _settingsPageModel;
		public SettingsPageModel SettingsPageModel
		{
			get => _settingsPageModel;
			set => SetProperty(ref _settingsPageModel, value);
		}

		private SummaryPageModel _summaryPageModel;
		public SummaryPageModel SummaryPageModel
		{
			get => _summaryPageModel;
			set => SetProperty(ref _summaryPageModel, value);
		}

		private TimeClockPageModel _timeClockPageModel;
		public TimeClockPageModel TimeClockPageModel
		{
			get => _timeClockPageModel;
			set => SetProperty(ref _timeClockPageModel, value);
		}

		public DashboardPageModel(ProfilePageModel profilePageModel, 
			SettingsPageModel settingsPageModel, 
			SummaryPageModel summaryPageModel, 
			TimeClockPageModel timeClockPageModel)
		{
			ProfilePageModel = profilePageModel;
			SettingsPageModel = settingsPageModel;
			SummaryPageModel = summaryPageModel;
			TimeClockPageModel = timeClockPageModel;
		}

		public override Task InitializeAsync(object navigationData = null)
		{
			return Task.WhenAny(base.InitializeAsync(navigationData),
				ProfilePageModel.InitializeAsync(null),
				SettingsPageModel.InitializeAsync(null),
				SummaryPageModel.InitializeAsync(null),
				TimeClockPageModel.InitializeAsync(null));
		}
	}
}
