﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimeTracker.PageModels.Base;
using TimeTracker.Services.Account;
using TimeTracker.Services.Statements;
using TimeTracker.Services.Work;
using TimeTracker.ViewModels;

namespace TimeTracker.PageModels
{
	public class SummaryPageModel : PageModelBase
	{
		private IStatementService _statementService;
		private IWorkService _workService;
		private IAccountService _accountService;
		private double _hourlyRate;

		private string _currentPayDateRange;
		public string CurrentPayDateRange
		{
			get => _currentPayDateRange;
			set => SetProperty(ref _currentPayDateRange, value);
		}

		private double _currentPeriodEarnings;
		public double CurrentPeriodEarnings
		{
			get => _currentPeriodEarnings;
			set => SetProperty(ref _currentPeriodEarnings, value);
		}

		private DateTime _currentPeriodPayDate;
		public DateTime CurrentPeriodPayDate
		{
			get => _currentPeriodPayDate;
			set => SetProperty(ref _currentPeriodPayDate, value);
		}

		private List<PayStatementViewModel> _statements;

		public List<PayStatementViewModel> Statements
		{
			get => _statements;
			set => SetProperty(ref _statements, value);
		}

		public SummaryPageModel(IStatementService statementService, IWorkService workService, IAccountService accountService)
		{
			_statementService = statementService;
			_workService = workService;
			_accountService = accountService;
		}

		public override async Task InitializeAsync(object navigationData)
		{
			_hourlyRate = await _accountService.GetCurrentPayRateAsync();
			var statements = await _statementService.GetStatementHistoryAsync();
			if (statements != null)
			{
				Statements = statements.Select(s => new PayStatementViewModel(s)).ToList();
				var lastStatement = statements.FirstOrDefault();
				if (lastStatement != null)
				{
					var today = DateTime.Now;
					var max = 100;
					var currentCount = 0;
					var currentEnd = lastStatement.EndDate;
					while (currentEnd < today && currentCount < max)
					{
						currentEnd = currentEnd.AddDays(14);
						currentCount++;
					}
					if (currentEnd > today && currentEnd.AddDays(-13) < today)
					{
						SetDateRange(currentEnd.AddDays(-13), currentEnd);
					}
				}

			}
			var currentPeriodItems = await _workService.GetWorkForThisPeriodAsync();
			foreach(var item in currentPeriodItems)
			{
				CurrentPeriodEarnings += item.Total.TotalHours * _hourlyRate;
			}
			await base.InitializeAsync(navigationData);
		}

		private void SetDateRange(DateTime startDate, DateTime endDate)
		{
			CurrentPayDateRange = startDate.ToString("MMMM d") + " - " + endDate.ToString("MMMM d, yyyy");
			CurrentPeriodPayDate = endDate.AddDays(6);
		}
	}
}
