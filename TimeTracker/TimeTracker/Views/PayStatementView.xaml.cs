﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeTracker.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PayStatementView : ContentView
	{
		public PayStatementView()
		{
			InitializeComponent();
		}
	}
}