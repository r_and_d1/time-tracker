﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeTracker.Views.Buttons
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BindableButton : Button
	{
		public BindableButton()
		{
			InitializeComponent();
		}
	}
}