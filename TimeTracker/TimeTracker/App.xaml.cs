﻿using System.Threading.Tasks;
using TimeTracker.PageModels;
using TimeTracker.Services.Navigation;
using TimeTracker.ViewModels.Base;
using Xamarin.Forms;

namespace TimeTracker
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();
		}

		Task InitNavigation()
		{
			var navService = PageModelLocator.Resolve<INavigationService>();
			return navService.NavigateToAsync<LoginPageModel>();
		}

		protected override async void OnStart()
		{
			await InitNavigation();
		}

		protected override void OnSleep()
		{
		}

		protected override void OnResume()
		{
		}
	}
}
